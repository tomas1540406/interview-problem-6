/** @format */

const fs = require("fs");

// get the files content
const actions = require("./input_files/problem_robot/actions.json");
const problem = require("./input_files/problem_robot/problem.json");
const states = require("./input_files/problem_robot/states.json");

// check if actions is array
if (!Array.isArray(actions.actions)) throw new Error("action is not array");

// get the next possible actions
function findNextActions(state_end, path) {
  let nextActions = actions.actions.filter((action) => {
    return action.state_start === state_end;
  });
  return (
    nextActions
      // sort by time
      .sort((a, b) => {
        return a.time - b.time;
      })
    // remove the actions already taken
    //  .filter((action) => {
    //    return !path.find((path) => path.currentState === action.state_end);
    //  })
  );
}

// find a path
// The path following the least time possible given by actions if infinite, it doesn't find a path
// Let's try again by keeping track of the actions already taken
function findActionPath(init, goal) {
  return new Promise((resolve, reject) => {
    // find a path
    let goalAchieved = false;
    let action = problem.init;
    let iteration = 0;
    let time = 0;
    let choice = 0; // default choice is the least time
    let path = [];

    const paths = [
      {
        time: time,
        action: action,
        next: [],
      },
    ];

    while (goalAchieved === false) {
      iteration++;

      // break if too many iterations
      if (iteration > 100) {
        reject("too many iterations");
        break;
      }

      // get the next possible actions
      let nextActions = findNextActions(currentState, path);

      // find all possible paths
      nextActions.map((action, i) => {
        console.log(`\t ${currentState} -> ${action.state_end}: ${action.time} ${i === choice ? "<- selected" : ""}`);

        paths.next.push({
          iteration: iteration,
          time: action.time,
          action: action.action,
          next: [],
        });
      });

      //console.log(paths);

      // break if no more actions
      if (nextActions.length === 0) {
        // BACKWARD

        if (choice >= path[path.length - 1].options.length - 1) {
          path.pop();
          choice = 0;
        } else {
          choice++;
        }

        throw new Error("no more actions");
        // set the last state as current and take the next choice
        nextActions = path[path.length - 1].options;
        iteration = path[path.length - 1].iteration;
        time = path[path.length - 1].time;
        //choice = path[path.length - 1].choice + 1;
        choice = choice >= nextActions.length ? 0 : choice++;

        console.log("######", iteration, nextActions.length, choice);

        // check if there is no more actions
        if (choice >= path[path.length - 1].options.length - 1) {
          // if no more actions, go back to the previous state
          // path.pop();
          path.pop();

          // set the last state as current and take the next choice
          nextActions = path[path.length - 1].options;
          iteration = path[path.length - 1].iteration;
          time = path[path.length - 1].time;
          // choice = 0;

          console.log("##################", iteration, nextActions.length, choice);

          // throw new Error("no more actions");
        }
        /*
        console.log("############", nextActions.length);
        console.log("############ last ", path[path.length - 1]);

        function rollback() {
          // if no more actions, go back to the previous state
          let previousState = path.pop();
          console.log("############", previousState);

          throw new Error("no more actions");

          if (!previousState) rollback();
          else {
            console.log("############", previousState.options[previousState.choice + 1]);
            currentState = previousState.action;
            time = previousState.time;
            iteration = previousState.iteration;
          }
        }
        rollback();
        */
      }
      // FORWARD
      // get the first action
      let nextAction = nextActions[choice];
      // increment time
      time += nextAction.time;
      // set the next state as current state
      currentState = nextAction.state_end;

      // print the current status
      console.log(`iteration: ${iteration}, time: ${time}`);
      console.log("options:");
      nextActions.map((action, i) => {
        console.log(`\t ${currentState} -> ${action.state_end}: ${action.time} ${i === choice ? "<- selected" : ""}`);
      });

      currentState = nextAction.state_end;

      // store the current state
      path.push({
        iteration: iteration,
        time: time,
        currentState: currentState,
        choice: choice,
        options: nextActions,
      });

      // reset choice if no more actions
      choice = 0; // reset choice
      iteration++;
    }

    resolve(path);
  });
}

function generatePathFile(filePath, path) {
  // generate the path file
  let pathFile = {
    actions: path.map((step) => {
      return step.options[step.choice].action;
    }),
    time: path[path.length - 1].time,
  };
  // write the file
  fs.writeFile(filePath, JSON.stringify(pathFile), (err) => {
    if (err) throw err;
    console.log("The file has been saved!");
  });
}

findActionPath(problem.init, problem.goal)
  .then((path) => {
    console.log("\nPath found:");

    generatePathFile("./output_files/plan.json", path);
  })
  .catch((err) => {
    console.log("\nError:", err);
  });
