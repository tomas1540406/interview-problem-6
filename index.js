/** @format */

const fs = require("fs");

// get the files content
const actions = require("./input_files/problem_robot/actions.json");
const problem = require("./input_files/problem_robot/problem.json");
const states = require("./input_files/problem_robot/states.json");

// check if actions is array
if (!Array.isArray(actions.actions)) throw new Error("action is not array");

// get the next possible actions
function findNextActions(state_end, path) {
  let nextActions = actions.actions.filter((action) => {
    return action.state_start === state_end;
  });
  return (
    nextActions
      // sort by time
      .sort((a, b) => {
        return a.time - b.time;
      })
  );
}

// find a path
// The path following the least time possible given by actions if infinite, it doesn't find a path
// Let's try again by keeping track of the actions already taken
// Let's try again by tryiong all the possible paths at once
function findActionPath(init, goal) {
  return new Promise((resolve, reject) => {
    // find a path
    let goalAchieved = false;
    let action = init;
    let time = 0;
    let iteration = 0;

    const paths = [
      {
        iteration: iteration,
        time: time,
        action: action,
        state_start: null,
        state_end: action,
        next: [],
      },
    ];

    const history = [];

    function nextOptions(next) {
      iteration++;

      next.map((p) => {
        // get the next possible actions
        let nextActions = findNextActions(p.state_end, []);

        nextActions.map((a, i) => {
          console.log(a.state_start, "->", a.state_end, ":", a.time);
        });
        console.log("nextActions", nextActions.length);
        // add the next possible actions to the path
        nextActions.map((a, i) => {
          if (action.state_end === goal) {
            goalAchieved = true;
            console.log(action);
            return;
          }

          // I could not find the path to the goal without getting an error :
          // Error: RangeError: Maximum call stack size exceeded
          if (iteration > 100) reject("too many iterations");

          // check if the action is already in the history
          if (!history.find((h) => h.action === a.action)) {
            history.push(a);

            p.next.push({
              iteration: iteration,
              time: a.time,
              action: a.action,
              state_start: a.state_start,
              state_end: a.state_end,
              next: [],
            });
          }
        });

        nextOptions(p.next);
      });
    }

    nextOptions(paths);

    console.log("paths", JSON.stringify(paths, null, 3));

    resolve(paths);
  });
}

/*
function generatePathFile(filePath, path) {
  // generate the path file
  let pathFile = {
    actions: path.map((step) => {
      return step.options[step.choice].action;
    }),
    time: path[path.length - 1].time,
  };
  // write the file
  fs.writeFile(filePath, JSON.stringify(pathFile), (err) => {
    if (err) throw err;
    console.log("The file has been saved!");
  });
}
*/

findActionPath(problem.init, problem.goal)
  .then((path) => {
    console.log("\nPath found:");

    // generatePathFile("./output_files/plan.json", path);
  })
  .catch((err) => {
    console.log("\nError:", err);
  });
